import { Routes } from '@angular/router';
import { AuthGuard } from './shared/service/guards/auth.guard.service';


export const appRoutes: Routes = [
    {
        path: '', redirectTo: 'page/home', pathMatch: 'full'
    },
    // Public zone
    {
        path: 'page',
        loadChildren: () => import('./web/web.module').then(module => module.WebModule)
    },
    // Admin zone
    {
        path: 'admin',
        canActivate: [AuthGuard],
        loadChildren: () => import('./admin/admin.module').then(module => module.AdminModule)
    },
];
