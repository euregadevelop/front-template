import { CoreService } from './services/core.service';
import { Component, OnInit } from '@angular/core';
import { DebugService, DebugLevel } from '@eurega/web-core';

@Component({
    selector: 'app-web',
    templateUrl: './web.component.pug',
    styleUrls: ['./web.component.scss']
})
export class WebComponent implements OnInit {
    constructor(private logger: DebugService, private coreService: CoreService) { }
    /** Global application init */
    public ngOnInit() {
        this.coreService.initPages();
    }
}
