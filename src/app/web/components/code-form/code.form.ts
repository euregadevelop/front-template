import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ObituaryService } from '../../services/obituary.service';
import { AuthService } from '@eurega/web-core';

@Component({
    selector: 'app-code-form',
    templateUrl: './code.form.pug',
    styleUrls: ['./code.form.scss']
})
export class CodeForm implements OnInit {
    public hide: boolean;
    public loginForm: FormGroup;
    public loginProcess = false;
    @ViewChild('saveButton', { read: ElementRef }) saveButton: ElementRef;

    constructor(
        private obituaryService: ObituaryService,
        private authService: AuthService,
        private fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<CodeForm>,
    ) {
        let code = "";
        this.loginForm = fb.group({
            code: new FormControl(code, [Validators.required])
        });
    }
    ngOnInit() {
        // Nos subscribimos al canal del store que nos informa de un login correcto
        this.authService.getUser().subscribe((user) => {
            if (user) {
                this.loginProcess = false;
                this.close();
            }
        });
    }
    public doLogin() {
        if (!this.loginProcess && this.loginForm.valid) {
            try {
                this.loginProcess = true;
                // this.authService.authUser(this.loginForm.value.code);
            } catch (err) {
                console.log('err: ', err);
                alert('Login error');
                this.loginProcess = false;
            }
        }
    }
    close() {
        this.dialogRef.close();
    }
}
