import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ObituariesComponent } from './obituary.component';

describe('ObituariesComponent', () => {
    let component: ObituariesComponent;
    let fixture: ComponentFixture<ObituariesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ObituariesComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(ObituariesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
