import { environment } from './../../../../environments/environment.dev';
import { Component, OnInit, Input, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-obituaries',
    templateUrl: './obituary.component.pug',
    styleUrls: ['./obituary.component.scss']
})
export class ObituariesComponent implements OnInit, OnDestroy, OnChanges {
    /** Section title to display */
    @Input() public title: string;
    /** Section subtitle to display */
    @Input() public subTitle: string;
    /** Obituaries to show */
    @Input() public items: any;
    /** Section background image */
    @Input() public bgImage: string;
    /** Component display options */
    @Input() public options: any;

    public unsubscriber$ = new Subject();
    public bgStyles: any;
    /** Server to allocate images */
    public imageBucket = environment.ImageBucket;
    public loading: boolean;

    constructor() { }

    public ngOnInit(): void { }

    public ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (
            changes.items &&
            changes.items.firstChange &&
            !changes.items.currentValue
        ) {
            this.loading = true;
        } else if (changes.items && !changes.items.firstChange) {
            this.loading = false;
        }
    }
}
