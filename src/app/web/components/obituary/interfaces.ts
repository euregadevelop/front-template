export declare interface IObituaryOptions {
    bgFixed: boolean;
    bgOptions: string;
    single: boolean;
}

export declare interface IObituary {
    id?: string;
    name?: string;
    imageUrl?: string;
    visitCode?: string;
    expedientCode?: string;
    isReady?: boolean;
    isActive?: boolean;
    isVisible?: boolean;
    createdAt?: Date;
    cementery?: any;
    cementeryId?: string;
    church?: any;
    churchId?: string;
    assets?: Array<any>;
    poemImage?: string;
    poemImageUrl?: string;
    poemText?: string;
}

export declare interface IDatatableObituary {
    id?: string;
    Nom?: string;
    Actiu?: string;
    Creat?: string;
    Completat?: string;
}
