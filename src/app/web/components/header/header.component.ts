import { takeUntil } from 'rxjs/operators';
import {
    Component, OnInit, ElementRef, HostListener, Inject, Injector, HostBinding, OnDestroy,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { AuthService, TemplatingClass } from '@eurega/web-core';
import { Subject } from 'rxjs';
import { headerTemplate } from './template';
import { WINDOW } from '../../../shared/helpers/window.helper';
import { CodeForm } from '../code-form/code.form';

@Component(
    {
        selector: 'app-header',
        templateUrl: './header.component.pug',
        styleUrls: ['./header.component.scss'],
    }
)
export class HeaderComponent extends TemplatingClass implements OnInit, OnDestroy {
    @HostBinding('class.menu-opened') menuOpened = false;
    // App actually active user
    public appUser: any = null;

    private unsubscriber$ = new Subject<boolean>();

    private headerTemplate = headerTemplate;
    /** Determina si el header está fijo */
    public isFixed: boolean;

    constructor(
        private injector: Injector,
        private loginDialog: MatDialog,
        private authService: AuthService,
        private element: ElementRef,
        @Inject(DOCUMENT) private document: Document,
        @Inject(WINDOW) private window: Window
    ) {
        super(injector);
    }
    /** Initialize component observables and templating */
    ngOnInit(): void {
        this.initialize(this.element, headerTemplate);
        this.authService.getUser().pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(user => {
            if (user) {
                this.appUser = user;
            } else {
                this.appUser = null;
            }
        });
    }
    /** Destroy all opened streams */
    ngOnDestroy(): void {
        this.unsubscriber$.next(true);
        this.unsubscriber$.complete();
    }
    /** Control of header fixed on top of the page */
    @HostListener('window:scroll', [])
    public onWindowScroll() {
        const offset = this.window.pageYOffset ||
            this.document.documentElement.scrollTop ||
            this.document.body.scrollTop ||
            0;
        if (offset > 15) {
            this.isFixed = true;
        } else {
            this.isFixed = false;
        }
    }
    /** Opens menu on mobile version */
    public toggleMenu() {
        this.menuOpened = !this.menuOpened;
    }

    public openSigninPopup() {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = false;
        dialogConfig.autoFocus = true;
        const dialogRef = this.loginDialog.open(CodeForm, dialogConfig);
    }
    public doLogIn() { }
    public doLogout() { }
}
