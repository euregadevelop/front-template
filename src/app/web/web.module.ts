import { HeaderComponent } from './components/header/header.component';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NguCarouselModule } from '@ngu/carousel';
import { HttpClientModule } from '@angular/common/http';

import { CoreModule } from '@eurega/web-core';

import { TranslateModule } from '@ngx-translate/core';

import { TranslatorPipe } from './../shared/pipes/translator';
import { TitleimageComponent } from './../shared/components/title-image/title.image.component';
import { BoxesListComponent } from './../shared/components/boxes-list/boxes.list.component';
import { ItemsListComponent } from './../shared/components/items-list/items.list.component';
import { SectionHeader } from './../shared/layout/section-header/section.header';
import { PortfolioComponent } from './../shared/components/portfolio/portfolio.component';
import { PricingComponent } from './../shared/components/pricing/pricing.component';
import { RoundedIconsComponent } from './../shared/components/rounded-icons/rounded.icons.component';
// import { HomeComponent } from './pages/home/home.component';
import { webRoutes } from './web.routing';
import { SharedMaterialModule } from './../shared/modules/material.module';
import { FooterComponent } from './components/footer/footer.component';
import { CodeForm } from './components/code-form/code.form';
import { BgImageComponent } from './../shared/components/bg-image/bgImage.component';
import { SigninComponent } from '../shared/sign-in/signin.component';
import { WebComponent } from './web.component';
import { ItemsListExplicationComponent } from '../shared/components/items-list-explication/items.list.explication.component';
import { HomeExampleComponent } from '../shared/examples/home/home.example';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        RouterModule.forChild(webRoutes),
        CoreModule,
        HttpClientModule,
        SharedMaterialModule,
        AngularFireDatabaseModule,
        NguCarouselModule,
    ],
    declarations: [
        FooterComponent,
        HeaderComponent,
        CodeForm,
        WebComponent,
        HomeExampleComponent,
        // HomeComponent,
        // Shared components
        BgImageComponent,
        RoundedIconsComponent,
        PricingComponent,
        PortfolioComponent,
        SigninComponent,
        SectionHeader,
        ItemsListComponent,
        BoxesListComponent,
        TitleimageComponent,
        ItemsListExplicationComponent,
        // Custom pipes
        TranslatorPipe
    ],
    entryComponents: [HeaderComponent, FooterComponent]
})
export class WebModule { }
