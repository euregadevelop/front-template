import { Injectable } from '@angular/core';
import { DebugService, TemplatingService, DebugLevel, ApiService } from '@eurega/web-core';
import { TranslateService } from '@ngx-translate/core';
import { templates } from '../../../themes/theme';
import { environment } from '../../../environments/environment';
import { ObituaryService } from './obituary.service';

@Injectable()
export class CoreService {
    // tslint:disable-next-line: max-line-length
    constructor(private obituaryService: ObituaryService, private api: ApiService, private debugService: DebugService, private translateService: TranslateService, private templateService: TemplatingService) { }

    /** Initialize global configuration for public pages */
    public initPages() {
        // Establecemos el theme activo por defecto
        this.templateService.setActiveTheme(templates, 'lightTheme');
        // Seteamos las variables css establecidas en el theme activado
        this.templateService.setElementTemplate(document.documentElement);
        this.debugService.debug(DebugLevel.INFO, 'template initialized', 'lightTheme');
        this.obituaryService.fetchAll().subscribe(
            _ => console.log
        );
    }

    /** Initialize app global configurations */
    public async initApp(): Promise<void> {
        // El nivel de debug establecido en el entorno
        this.debugService.setDebugLevel(environment.DebugLevel);
        this.debugService.debug(DebugLevel.INFO, `Debug level established to: `, environment.DebugLevel);
        // El lenguaje por defecto
        this.translateService.setDefaultLang('en');
        const userLang: string = this.translateService.getBrowserLang();
        this.translateService.use(userLang);
        this.debugService.debug(DebugLevel.INFO, `Language default setted to`, userLang);
        // Iniciamos las conexiones con la api
        this.api.setAPiUrl(environment.apiURL);
        this.debugService.debug(DebugLevel.INFO, `Api route setted to: `, environment.apiURL);
    }
}
