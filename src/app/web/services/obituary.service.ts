import { Injectable, OnDestroy } from '@angular/core';
import { ApiService } from '@eurega/web-core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { tap, shareReplay, takeUntil } from 'rxjs/operators';
import { IObituary } from '../components/obituary/interfaces';

@Injectable()
export class ObituaryService implements OnDestroy {
    /** Problems stream to live reload by subscribing it from templates or controllers */
    private obituaries$ = new BehaviorSubject<Array<IObituary>>([]);
    private unsubscriber$ = new Subject<boolean>();

    constructor(private apiService: ApiService) { }

    /** unsubscribe from all streams */
    ngOnDestroy(): void {
        this.unsubscriber$.next(true);
        this.unsubscriber$.complete();
    }

    public getVisible(): Array<IObituary> {
        const allItems = this.obituaries$.getValue();
        return allItems.filter((obituary: IObituary) => {
            return obituary.isVisible;
        });
    }

    /** Get only active obiruaries */
    public getActive(): Array<IObituary> {
        const allItems = this.obituaries$.getValue();
        return allItems.filter((obituary: IObituary) => {
            return obituary.isActive;
        });
    }

    /** Get all obituaries and store it on service */
    public fetchAll(): Observable<IObituary> {
        return this.apiService.getAll('obituary').pipe(
            takeUntil(this.unsubscriber$),
            tap((obituaries) => {
                this.obituaries$.next(obituaries);
            })
        );
    }

    /** Returns observable problems list */
    public getAsObservable(): Observable<Array<IObituary>> {
        return this.obituaries$.asObservable().pipe(
            takeUntil(this.unsubscriber$),
            shareReplay(1)
        );
    }
    /**
     * Get obituary by id on stored obituaries
     */
    public getStoredById(id: string): IObituary {
        const allItems = this.obituaries$.getValue();
        return allItems.find((obituary: IObituary) => {
            return obituary.id.toString() === id.toString();
        });
    }
    /**
     * Get obituary by client code
     * @param code Client code
     */
    public getObituaryByCode(code: string): IObituary {
        const allItems = this.obituaries$.getValue();
        return allItems.find((obituary: IObituary) => {
            return obituary.visitCode && obituary.visitCode.toString() === code.toString();
        });
    }
}
