import { Routes } from '@angular/router';
// import { HomeComponent } from './pages/home/home.component';
import { WebComponent } from './web.component';
import { HomeExampleComponent } from '../shared/examples/home/home.example';

export const webRoutes: Routes = [
    {
        path: '',
        component: WebComponent,
        children: [
            { path: 'home', component: HomeExampleComponent }
        ]
    }
];
