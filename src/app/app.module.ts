import { AngularFireDatabaseModule } from '@angular/fire/database';
// Angular core
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes';
// Firebase
import { AngularFireModule } from "@angular/fire";
// Shared
import { WINDOW_PROVIDERS } from './shared/helpers/window.helper';
import { TranslateModule, TranslateLoader, } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { environment } from './../environments/environment';
import { CoreModule } from '@eurega/web-core';
import { ObituaryService } from './web/services/obituary.service';
import { RootComponent } from './root.component';
import { CoreService } from './web/services/core.service';
import { LocalStorageService } from './shared/data-sources/localstorage.service';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './../assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        RootComponent
    ],
    imports: [
        BrowserAnimationsModule,
        RouterModule.forRoot(appRoutes, { useHash: true, initialNavigation: true }),
        CoreModule,
        BrowserModule,
        HttpClientModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            },
            isolate: false
        }),
    ],
    providers: [
        WINDOW_PROVIDERS,
        LocalStorageService,
        ObituaryService,
        CoreService
    ],
    bootstrap: [RootComponent]
})
export class AppModule { }
