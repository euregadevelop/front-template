import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({ name: 'translator' })
export class TranslatorPipe implements PipeTransform {
    constructor(private translateService: TranslateService) {}
    transform(value: string | Array<string>, lang?: string): string {
        if (lang) {
            return value[lang] ? value[lang] : 'No traduction found';
        } else {
            const currentLang = this.translateService.currentLang;
            const defaultLang = this.translateService.getDefaultLang();
            return value[currentLang] ? value[currentLang] : (value[defaultLang] ? value[defaultLang] : 'No traduction found');
        }
    }
}
