export declare interface i18nField {
    en?: string,
    es?: string,
    ca?: string
}

export declare interface ImageField {
    src: string;
    alt: string;
}

export enum TLayered {
    soft = 'soft',
    medium = 'medium',
    dark = 'dark',
    none = 'none'
}