import { Injectable, OnInit } from '@angular/core';
/** Get all the dynamic page content from content api (generally firebase) */
@Injectable()
export class ContentFirebaseService implements OnInit {

    constructor() { }

    // tslint:disable-next-line: contextual-lifecycle
    public ngOnInit(): void { }
}
