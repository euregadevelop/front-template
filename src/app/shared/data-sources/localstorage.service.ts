import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

const PREFIX: string = environment.APP_LOCALSTORAGE_PREFIX;
/**
 * Servicio que se encarga de gestionar los datos guardados en el localstorage del navegador
 */
@Injectable()
export class LocalStorageService {
    constructor() { }
    /** Insertamos un valor en el localstorage */
    setItem(key: string, value: any) {
        localStorage.setItem(`${PREFIX}${key}`, JSON.stringify(value));
    }

    getItem(key: string) {
        return JSON.parse(localStorage.getItem(`${PREFIX}${key}`));
    }

    removeItem(key: string) {
        localStorage.removeItem(`${PREFIX}${key}`);
    }
    /** Comprobamos si el localsotarge es accesible */
    testLocalStorage() {
        const testValue = 'testValue';
        const testKey = 'testKey';
        let retrievedValue: string;
        const errorMessage = 'localStorage did not return expected value';

        this.setItem(testKey, testValue);
        retrievedValue = this.getItem(testKey);
        this.removeItem(testKey);

        if (retrievedValue !== testValue) {
            throw new Error(errorMessage);
        }
    }

}
