import { Component, OnInit, ViewChild, ElementRef, Inject, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '@eurega/web-core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.pug',
    styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit, OnDestroy {
    public hide: boolean;
    public loginForm: FormGroup;
    public loginProcess = false;
    @ViewChild('saveButton', { read: ElementRef }) saveButton: ElementRef;
    @ViewChild('loginGoogleButon') loginGoogleButton: ElementRef;

    constructor(
        private authService: AuthService,
        private fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<SigninComponent>,
    ) {
        const email = '';
        const password = '';

        // Creación del formulario que se mostrará en el template
        this.loginForm = fb.group({
            email: new FormControl(email, [Validators.required, Validators.email]),
            password: new FormControl(password, [Validators.required, Validators.minLength(5)])
        });
    }
    ngOnInit() {
        // Nos subscribimos al canal del store que nos informa de un login correcto
        this.authService.getUser().subscribe((user) => {
            if (user) {
                this.loginProcess = false;
                this.close();
            }
        });
    }
    public async doLogin() {
        if (!this.loginProcess) {
            this.loginProcess = true;
            await this.authService.Login(this.loginForm.value).toPromise();
            this.loginProcess = false;
        }
    }
    onGoogleLogin() {
        this.authService.GoogleLogin();
    }
    close() {
        this.dialogRef.close();
    }
    ngOnDestroy() {

    }

}
