import { i18nField, ImageField, TLayered } from '../../typings/common';

export interface TBoxesListComponent {
    boxesListItems: Array<Partial<IBoxItem>>;
    boxesListTitle: string;
    boxesListSubTitle: string;
    boxesListOptions: IOptions;
    boxesListBgImage: string;
}

export declare interface IOptions {
    /** Opciones de estilo para el background, por defecto se le asigna el valor  'center center / cover'  */
    bgOptions: string;
    /** Determina si el background mostrará el efecto 'paralax'  */
    bgFixed: boolean;
    /** Este componente solo mostrará un layer soft en caso de recibir este parámetro */
    layered: TLayered;
}

export declare interface IBoxItem {
    /** Si el item no incorpora 'link', este campo contiene la id del elemento html al que ir cuando se hace click */
    rel: string;
    /** Si el item no tiene 'ref' este campo nos lleva a un link externo o interno  */
    link: string;
    /** Ruta y texto alt a la imágen del item */
    image: ImageField;
    /** Título traducible del item */
    title: i18nField;
    /** Descripción traducible que se muestra debajo del título */
    description: i18nField;
}
