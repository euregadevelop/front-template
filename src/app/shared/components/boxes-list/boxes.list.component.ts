import { Component, OnInit, Input, OnDestroy, Injector, ElementRef } from '@angular/core';
import * as componentInterfaces from './interfaces';
import { Subject } from 'rxjs';
import { IBoxItem } from './interfaces';
import { TemplatingClass } from '@eurega/web-core';
import { serviceBoxTemplate, componentTemplate } from './template';

@Component({
    selector: 'app-boxes-list',
    templateUrl: './boxes.list.component.pug',
    styleUrls: ['./boxes.list.component.scss']
})
export class BoxesListComponent extends TemplatingClass implements OnInit, OnDestroy {
    /** Items que se mostrarán */
    @Input() public items: Array<IBoxItem>;
    /** Título de la sección (opcional) */
    @Input() public title?: string;
    /** Subtítulo de la sección (opcional) */
    @Input() public subTitle?: string;
    /** Imágen de fondo para el componente (opcional) */
    @Input() public bgImage?: string;
    /** Opciones de configuración para el componente */
    @Input() public options?: componentInterfaces.IOptions;
    /** Template para definir los estilos del componente (opcional,  si no se recibe se usa el template por defecto) */
    @Input() public template?: serviceBoxTemplate;
    /** Lenguaje por defecto de la aplicación (opcional) */
    public lang: string;

    public unsubscriber$ = new Subject();
    /** Define los estilos para el background */
    public bgStyles: any;
    /** Controla el spinner  */
    public loading: boolean;

    constructor(private injector: Injector, private element: ElementRef) {
        super(injector);
    }

    public ngOnInit(): void {
        this.initialize(this.element, componentTemplate, this.template);
        // opciones por defecto en caso de no llegar definidas
        if (!this.options.bgOptions) {
            this.options.bgOptions = 'center center / cover';
        }
        // Definimos los estilos del background
        this.bgStyles = { background: 'url(' + this.bgImage + ')' + this.options.bgOptions };
    }

    public ngOnDestroy(): void {
        this.unsubscriber$.next();
    }
    /**
     * Go to  DOM item id = #anchor
     * @param anchor Id del elemento html al que navegar al hacer click
     */
    public goToRel(anchor: string): void {
        const element = document.getElementById(anchor);
        element.scrollIntoView();
    }
}
