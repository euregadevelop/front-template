```json
item = {
    "img": {
            "src": "...",
            "alt": "..."
        },
        "title": {
            "lang": "..."
        },
        "description": {
            "lang": "..."
        }
    }
}
```

```cs
# available options:
# ------------------

- layered (boolean) : Cuando enviamos true para este parámetro se coloca un filtro semitransparente que difumina un poco la imágen de fondo.

- bgOptions (string): 
// example: 'center center / cover'
