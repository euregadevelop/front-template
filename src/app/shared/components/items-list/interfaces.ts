import { i18nField, ImageField } from '../../typings/common';

export declare interface ItemListElement {
    image: ImageField;
    description: i18nField | Array<string>;
    title: i18nField;
}
