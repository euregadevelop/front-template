/** Default value for component template */
export const componentTemplate: itemsListTemplate = {
    colors: {
        "--item-title-color": "var(--dark-blue)",
        "--item-text-color": "var(--dark-blue)",
        "--image-border-color": "var(--grey-medium)"
    },
};
/** Definition of rounded icons component template */
export declare interface itemsListTemplate {
    colors: itemsListColors;
}

declare interface itemsListColors {
    "--item-title-color"?: string;
    "--item-text-color"?: string;
    "--image-border-color"?: string;
}
