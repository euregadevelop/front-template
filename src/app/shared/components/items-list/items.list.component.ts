import { Component, OnInit, Input, OnDestroy, Injector, ElementRef } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TemplatingClass } from '@eurega/web-core';
import { itemsListTemplate, componentTemplate } from './template';
import { ItemListElement } from './interfaces';

@Component({
    selector: 'app-items-list',
    templateUrl: './items.list.component.pug',
    styleUrls: ['./items.list.component.scss']
})
export class ItemsListComponent extends TemplatingClass implements OnInit, OnDestroy {
    @Input() public items: Array<ItemListElement>;
    @Input() public title: string;
    @Input() public subTitle: string;
    @Input() public options?: any;
    @Input() public bgImage?: string;
    @Input() public template?: itemsListTemplate;
    public unsubscriber$ = new Subject();
    /** Variable donde se decide el background image */
    public bgStyles: any;

    constructor(private injector: Injector, private element: ElementRef) {
        super(injector);
    }

    public ngOnInit(): void {
        this.initialize(this.element, componentTemplate, this.template);
        if (!this.options) {
            this.options = { bgOptions: 'center center / cover' };
        }
        if (this.options && !this.options.bgOptions) {
            this.options.bgOptions = 'center center / cover';
        }
        if (this.bgImage) {
            this.bgStyles =  {background: 'url(' + this.bgImage + ')' + this.options.bgOptions };
        }
    }
    public ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }
    /**
     * @returns true if is array, false elsewhere
     */
    public isArray(obj: any): boolean {
        return Array.isArray(obj);
    }
}
