import { titleImageTemplate } from './template';

export class TTitleImageComponent {
    public titleImageTitle: string;
    public titleImageSubTitle: string;
    public titleImageOptions: IOptions;
    public titleImageBgImage: string;
    public titleImageTitleImage: string;
    public titleImageTemplate: titleImageTemplate;
}
/**
 * @description opciones de configuración para el componente
 */
export enum ImagePositions {
    right = 'right',
    left = 'left'
}
export interface IOptions {
    // Nos dice si el fondo estará fijo o no durante el scroll
    bgFixed?: boolean;
    bgPosition?: ImagePositions;
}
