import { Component, OnInit, Input, ElementRef, Injector } from '@angular/core';
import * as componentInterfaces from './interfaces';
import { TemplatingClass } from '@eurega/web-core';
import { componentTemplate, titleImageTemplate } from './template';

@Component({
    selector: 'app-title-image',
    templateUrl: './title.image.component.pug',
    styleUrls: ['./title.image.component.scss']
})
export class TitleimageComponent extends TemplatingClass implements OnInit {
    @Input() public bgImage: string;
    @Input() public titleImage: string;
    @Input() public subtitle: string;
    @Input() public title: string;
    @Input() public options?: componentInterfaces.IOptions;
    @Input() public template?: titleImageTemplate;

    constructor(private injector: Injector, private element: ElementRef) {
        super(injector);
    }

    public ngOnInit(): void {
        this.initialize(this.element, componentTemplate, this.template);
        if (!this.options.bgPosition) {
            this.options.bgPosition = componentInterfaces.ImagePositions.left;
        }
    }
}
