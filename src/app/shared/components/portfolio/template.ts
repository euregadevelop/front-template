/** Default value for component template */
export const componentTemplate: portfolioTemplate = {
    colors: {
        "--button-border-color": "var(--dark-blue)",
        "--button-color": "var(--dark-blue)",
        "--item-title-color": "var(--grey-medium)",
        "--item-text-color": "var(--grey-medium)"
    },
};
/** Definition of rounded icons component template */
export declare interface portfolioTemplate {
    colors: portfolioColors;
}

declare interface portfolioColors {
    "--button-border-color"?: string;
    "--button-color"?: string;
    "--item-title-color"?: string;
    "--item-text-color"?: string;
}
