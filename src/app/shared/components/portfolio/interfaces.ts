import { i18nField, ImageField } from '../../typings/common';

export declare interface IPortfolioItem {
    image?: ImageField;
    description?: i18nField;
    title?: i18nField;
    order?: number;
    logo?: ImageField;
    link?: string;
}
