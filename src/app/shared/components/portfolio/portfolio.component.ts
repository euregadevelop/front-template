import { Component, OnInit, Input, Injector, ElementRef, ChangeDetectorRef } from '@angular/core';
import { componentTemplate, portfolioTemplate } from './template';
import { TemplatingClass } from '@eurega/web-core';
import { IPortfolioItem } from './interfaces';
import { NguCarousel, NguCarouselConfig } from '@ngu/carousel';
import { MatBottomSheet } from '@angular/material/bottom-sheet';

@Component({
    selector: 'app-portfolio',
    templateUrl: './portfolio.component.pug',
    styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent extends TemplatingClass implements OnInit {
    @Input() public title: string;
    @Input() public subTitle: string;
    /** Items to render on component */
    @Input() public items: Array<IPortfolioItem>;
    @Input() public options?: any;
    @Input() public template?: portfolioTemplate;

    public carouselOptions: NguCarouselConfig;

    public constructor(
        private bottomSheet: MatBottomSheet,
        private _cdr: ChangeDetectorRef,
        private injector: Injector,
        private element: ElementRef) {
        super(injector);
    }

    public ngOnInit(): void {
        this.initialize(this.element, componentTemplate, this.template);
        this.items.sort((a, b) => a.order - b.order)
        this.carouselOptions = {
            grid: { xs: 1, sm: 2, md: 3, lg: 3, all: 0 },
            slide: 2,
            speed: 400,
            interval: null,
            point: {
                visible: true
            },
            load: 2,
            touch: true,
            loop: false
        }
    }

    public ngAfterViewInit(): void {
        this._cdr.detectChanges();
    }

    public openShareComponent(): void { }

}
