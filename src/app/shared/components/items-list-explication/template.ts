/** Default value for component template */
export const componentTemplate: ListExplicationTemplate = {
    colors: {
        '--button-border-color': 'var(--dark-blue)',
        '--button-color': 'var(--dark-blue)',
        '--item-title-color': 'var(--grey-medium)',
        '--item-text-color': 'var(--grey-medium)'
    },
};
/** Definition of rounded icons component template */
export declare interface ListExplicationTemplate {
    colors: ListExplicationColors;
}

declare interface ListExplicationColors {
    '--button-border-color'?: string;
    '--button-color'?: string;
    '--item-title-color'?: string;
    '--item-text-color'?: string;
}
