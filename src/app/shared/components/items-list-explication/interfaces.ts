import { ImageField, i18nField } from '../../typings/common';
import { ListExplicationTemplate } from './template';

export interface TItemsListExplicationComponent {
    items: Array<Partial<IListExplicationItem>>;
   options: Partial<IOptions>;
   bgImage: string;
    template?: ListExplicationTemplate;
}

export interface IOptions {
    bgFixed: boolean;
    bgOptions: any;
    listType: TListType;
}

export interface IListExplicationItem {
    img: ImageField;
    title: i18nField;
    description: i18nField;
}

export enum TListType {
    litleImage = 'litleImage',
    enumList = 'enumList'
}