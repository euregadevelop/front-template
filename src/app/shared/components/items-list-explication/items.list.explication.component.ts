import {Component, OnInit, Input, OnDestroy, Injector } from '@angular/core';
import { TemplatingClass } from '@eurega/web-core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as componentInterfaces from './interfaces';
import { bgImageTemplate } from '../bg-image/template';

@Component({
    selector: 'app-items-list-explication',
    templateUrl: './items.list.explication.pug',
    styleUrls: ['./items.list.explication.scss']
})
export class ItemsListExplicationComponent extends TemplatingClass implements OnInit, OnDestroy {
    /** Items to show in component */
    @Input() public items: Array<componentInterfaces.IListExplicationItem>;
    /** Custom component options */
    @Input() public options?: Partial<componentInterfaces.IOptions>;
    /** Url for background image */
    @Input() public bgImage?: string;
    /** Template que contiene las variables usadas para estilizar el componente */
    @Input() public template?: bgImageTemplate;
    public unsubscriber$ = new Subject<boolean>();
    public bgStyles: any;

    constructor(private injector: Injector) {
        super(injector);
     }

    ngOnInit(): void {
        if (!this.options) {
            this.options = { bgOptions: 'center center / cover' };
        }
        if (this.options && !this.options.bgOptions) {
            this.options.bgOptions = 'center center / cover';
        }
        if (this.bgImage) {
            this.bgStyles = { background: 'url(' + this.bgImage + ')' + this.options.bgOptions };
        }
    }
    ngOnDestroy() {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }
    public isArray(obj: any): boolean {
        return Array.isArray(obj);
    }
}
