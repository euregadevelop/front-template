import { i18nField } from '../../typings/common';

export declare interface IPricingItem {
    'title': i18nField;
    'description': i18nField;
    'strong': string;
    'includes': Array<i18nField>;
    'details': Array<i18nField>;
}
