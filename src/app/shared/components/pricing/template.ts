/** Default value for component template */
export const componentTemplate: pricingTemplate = {
    colors: {
        "--item-title-separator": "var(--dark-blue)",
        "--item-title-color": "var(--dark-blue)",
        "--item-description-color": "var(--grey-medium)",
        "--pricing-detail-text-color": "var(--grey-medium)",
        "--pricing-includes-color": "var(--background-dark)",
    },
};
/** Definition of rounded icons component template */
export declare interface pricingTemplate {
    colors: pricingColors;
}

declare interface pricingColors {
    "--item-title-separator"?: string;
    "--item-title-color"?: string;
    "--item-description-color"?: string;
    "--pricing-detail-text-color"?: string;
    "--pricing-includes-color"?: string;
}
