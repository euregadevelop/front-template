import { Component, OnInit, Input, ElementRef, Injector } from '@angular/core';
import { componentTemplate, pricingTemplate } from './template';
import { TemplatingClass } from '@eurega/web-core';

@Component({
    selector: 'app-pricing',
    templateUrl: './pricing.component.pug',
    styleUrls: ['./pricing.component.scss']
})
export class PricingComponent extends TemplatingClass implements OnInit {
    /**  */
    @Input() public title: string;
    @Input() public subTitle: string;
    @Input() public items: Array<any>;
    @Input() public template?: pricingTemplate;

    constructor(private injector: Injector, private element: ElementRef) {
        super(injector);
    }

    public ngOnInit(): void {
        this.initialize(this.element, componentTemplate, this.template);
    }

}
