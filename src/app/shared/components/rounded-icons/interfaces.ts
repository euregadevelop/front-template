import { i18nField } from '../../typings/common';

export declare interface RoundedIcon {
    icon: string;
    title: i18nField;
    subtitle: i18nField;
}

export enum iconType {
    icon = 'icon',
    image = 'image'
}
export interface Options {
    iconType?: iconType;
}
