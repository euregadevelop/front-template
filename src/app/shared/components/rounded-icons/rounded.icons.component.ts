import { Component, OnInit, Input, OnDestroy, ElementRef, Injector } from '@angular/core';
import { Subject } from 'rxjs';
import * as ComponentInterfaces from './interfaces';
import { RoundedIcon } from './interfaces';
import { roundedIconsTemplate, componentTemplate } from './template';
import { TemplatingClass } from '@eurega/web-core';

@Component({
    selector: 'app-rounded-icons',
    templateUrl: './rounded.icons.component.pug',
    styleUrls: ['./rounded.icons.component.scss']
})
export class RoundedIconsComponent extends TemplatingClass implements OnInit, OnDestroy {
    @Input() public items: Array<RoundedIcon>;
    /** Section title */
    @Input() public title?: string;
    /** Section subtitle */
    @Input() public subTitle?: string;
    /** Component options */
    @Input() public options?: ComponentInterfaces.Options;
    /** Colors and parameters to render the component */
    @Input() public template?: roundedIconsTemplate;
    public unsubscriber$ = new Subject();

    public constructor(private injector: Injector, private element: ElementRef) {
        super(injector);
    }

    public ngOnInit(): void {
        this.initialize(this.element, componentTemplate, this.template);
        if (!this.options || !this.options.iconType) this.options = { iconType: ComponentInterfaces.iconType.icon };
    }

    public ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }
}
