/** Default value for component template */
export const componentTemplate: roundedIconsTemplate = {
    colors: {
        "--background-color-component": "var(--background-light)",
        "--border-color": "var(--background-dark)",
        "--icon-color": "var(--background-dark)",
        "--title-color": "var(--background-dark)",
        "--subtitle-color": "var(--background-dark)",
    },
    "background": "medium"
};
/** Definition of rounded icons component template */
export declare interface roundedIconsTemplate {
    colors: roundedIconsColors;
    "background"?: string;
}

declare interface roundedIconsColors {
    "--background-color-component"?: string;
    "--border-color"?: string;
    "--icon-color"?: string;
    "--title-color"?: string;
    "--subtitle-color"?: string;
}
