import { i18nField } from '../../typings/common';

// Opciones de configuración que el componente espera
export interface Options {
    // Si el componente es extended, decide si mostrar imágenes o iconos
    iconType?: iconType;
    // Tipo de componente a renderizar (solo imagen, imagen + título o imágen, título y contenido)
    componentType?: EComponentType;
    // Actualmente solo toma el valor 'solid' para estilizar los items en caso de recibirlos
    style?: any;
    // Determina la opcidad de la imágen de fondo
    filter?: EFilters;
}

/**
 * @description opciones de configuración para el componente
 */
export enum iconType {
    image = 'image',
    icon = 'icon'
}
export enum EComponentType {
    // Only bg image and title + subtitle
    onlyTitle = 'onlyTitle',
    // A bg image and title + subtitle + pharagraph text
    titleExtended = 'titleExtended',
    // title extended component + content items
    titleContent = 'titleContent'
}
export declare interface TComponentType {
    // puede ser el nombre de un material icon o bien la ruta de una imágen
    icon: string;
    title: i18nField;
    text: i18nField;
}
export interface Button {
    icon: string;
    text: string;
}

export enum EFilters {
    soft = 'soft',
    medium = 'medium',
    dark = 'dark'
}
