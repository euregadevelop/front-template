/** Default values for component template */
export const componentTemplate: bgImageTemplate = {
    colors: {
        "--title-color": "var(--text-background-dark)",
        "--subtitle-color": "var(--text-background-dark)",
        "--text-color": "var(--text-background-dark)",
    },
    "filter": "medium"
};

export declare interface bgImageTemplate {
    /** Colors used in component */
    colors: bgImageColors;
    /** Opaque filter for background image in component */
    "filter"?: string;
}
declare interface bgImageColors {
    "--title-color"?: string;
    "--subtitle-color"?: string;
    "--text-color"?: string;
}
