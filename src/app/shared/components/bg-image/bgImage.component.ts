import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, ElementRef, Injector } from '@angular/core';
import * as componentInterfaces from './interfaces';
import { Subject } from 'rxjs';
import { componentTemplate, bgImageTemplate } from './template';
import { TemplatingClass } from '@eurega/web-core';
import { EFilters } from './interfaces';

@Component({
    selector: 'app-bgimage',
    templateUrl: './bgImage.component.pug',
    styleUrls: ['./bgImage.component.scss']
})
export class BgImageComponent extends TemplatingClass implements OnInit, OnDestroy {
    /** Variable pendiente de revisión, que se usa pra mostrar contenido extra  */
    @Input() public items?: Array<componentInterfaces.TComponentType>;
    /** Ruta a la imágen que se usará como fondo */
    @Input() public bgImage: string;
    /** El texo que aparece sobre del título */
    @Input() public subtitle: string;
    /** El título principal del componente */
    @Input() public title: string;
    /** Texto que aparece debajo del título principal */
    @Input() public text: string;
    /** Template que contiene las variables usadas para estilizar el componente */
    @Input() public template?: bgImageTemplate;
    /** Ruta a una imágen que se puede mostrar debajo del título (actualmente sin uso) */
    @Input() public titleImage?: string;
    /** opciones de configuración del componente */
    @Input() public options?: componentInterfaces.Options;

    public unsubscriber$ = new Subject();

    constructor(private injector: Injector, private element: ElementRef) {
        super(injector);
    }

    public ngOnInit(): void {
        this.initialize(this.element, componentTemplate, this.template);
        console.log('lang selected: ', this.lang);
        // Tipo de componente por defecto
        this.options = {
            componentType:
                this.options && this.options.componentType ?
                    this.options.componentType :
                    componentInterfaces.EComponentType.onlyTitle,
            iconType:
                this.options && this.options.iconType ?
                    this.options.iconType :
                    componentInterfaces.iconType.icon,
            filter: this.options && this.options.filter ? this.options.filter : EFilters.soft
        }
    }
    /** Destroy component and all onpened streams */
    public ngOnDestroy(): void {
        this.unsubscriber$.next();
    }

}
