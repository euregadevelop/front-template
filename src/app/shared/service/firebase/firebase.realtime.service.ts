import { AngularFireObject } from '@angular/fire/database/database';
import { DebugService, DebugLevel } from '@eurega/web-core';
import { Subject, Observable } from 'rxjs';
import { Injectable, OnDestroy } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

export const enum RealTimeType {
    object = 'object',
    array = 'array'
}
@Injectable()
export class FirebaseRealTimeService implements OnDestroy {
    /** Arrays of references */
    private listReferences: object = {};
    private objectReferences: object = {};
    private databaseRoot: string = null;
    /** Guarda la referencia a la base de datos principal */
    private pageContentReference: AngularFirestoreCollection<any>;
    /** Unsubscribe from all streams */
    public unsubscriber$ = new Subject<boolean>();

    constructor(
        private debugService: DebugService, 
        private realTimeDatabase: AngularFireDatabase, 
        private firestore: AngularFirestore
    ) { }
    /**
     * Devuelve una referencia de tipo firestore collection a partir de un nombre
     * // TODO: catch and manage errors
     * @param name Name of the entry in realtime database
     */
    public getObjectReference(name: string): AngularFireObject<unknown> {
        return this.realTimeDatabase.object(name);
    }
    public getLists(): object {
        return this.listReferences;
    }
    public getObjects(): object {
        return this.objectReferences;
    }
    public setDatabaseRoot(databaseRoot: string) {
        this.databaseRoot = '/' + databaseRoot;
    }
    public setListReferences(references: Array<string>): void {
        if (references.length > 0) {
            references.forEach((reference: string) => {
                const newReference = this.getListReference(this.databaseRoot + '/' + reference + '/items');
                this.listReferences[reference] = newReference;
            });
        }
    }
    public setObjectReferences(references: Array<string>): void {
        if (references.length > 0) {
            references.forEach((reference: string) => {
                const newReference = this.getObjectReference(this.databaseRoot + '/' + reference + '/items');
                this.objectReferences[reference] = newReference;
            });
        }
    }
    public getListReference(name: string) {
        return this.realTimeDatabase.list(name);
    }
    /**
     * Establece el stream a la base de datos que proporciona el contenido principal de la aplicación
     * @param name Name of the entry in realtime database
     * @param type Type of data we want to receive
     */
    public getEntryAsObservable(entryRef): Observable<any> {
        this.debugService.debug(DebugLevel.TRACE, 'FirebaseService[ getEntryAsObservable ]: ', 'init');
        return entryRef.valueChanges();
    }
    /** Unsubscribe from all service streams */
    public ngOnDestroy(): void {
        this.unsubscriber$.next(true);
        this.unsubscriber$.complete();
    }
}
