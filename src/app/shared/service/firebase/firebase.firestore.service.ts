import { DebugService, DebugLevel } from '@eurega/web-core';
import { takeUntil, shareReplay, map } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { Injectable, OnDestroy } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable()
export class FirebaseFirestoreService implements OnDestroy {
    /** Guarda la referencia a la base de datos principal */
    private pageContentReference: AngularFirestoreCollection<any>;
    /** Unsubscribe from all streams */
    public unsubscriber$ = new Subject<boolean>();

    constructor(private debugService: DebugService, private realTimeDatabase: AngularFireDatabase, private firestore: AngularFirestore ) { }
    /**
     * Devuelve una referencia de tipo firestore collection a partir de un nombre
     * // TODO: catch and manage errors
     * @param name Name of the entry in realtime database
     */
    public getCollectionReference(name: string): AngularFirestoreCollection<any> {
        this.pageContentReference = this.firestore.collection(name);
        return this.pageContentReference;
    }
    /**
     * Establece el stream a la base de datos que proporciona el contenido principal de la aplicación
     * @param name Name of the entry in realtime database
     */
    public getEntryAsObservable(name: string): Observable<any> {
        this.debugService.debug(DebugLevel.TRACE, 'FirebaseService[ getEntryAsObservable ]: ', 'init');
        const collection = this.firestore.collection(name);
        return collection.snapshotChanges().pipe(
            shareReplay(1),
            map((items) => {
                return items.map(item => {
                    const data: any = item.payload.doc.data();
                    const id = item.payload.doc.id;
                    return { id, ...data };
                })
            })
        );
    }
    /**
     * Guarda en la base de datos principal de la aplicación un nuevo item
     * @param name Name of the entry in realtime database
     * @param item Objecto a guardar en la base de datos
     */
    public async addItem(name: string, item: any): Promise<void> {
        this.debugService.debug(DebugLevel.TRACE, 'FirebaseService[ addItem ]: ', 'init');
        const collection = this.firestore.collection(name);
        const id = this.firestore.createId();
        const newItem = { id, ...item };
        this.debugService.debug(DebugLevel.DEBUG, 'Item to create:  ', newItem);
        try {
            await this.pageContentReference.doc(id).set(newItem);
            this.debugService.debug(DebugLevel.TRACE, 'FirebaseService[ addItem ]: ', 'end');
        } catch (error) {
            this.debugService.debug(DebugLevel.DEBUG, 'Error creating firebase item. ', error);
            this.debugService.debug(DebugLevel.ERROR, 'Error creating firebase item. ', null);
        }
    }

    /**
     * Update de un item en la base de datos principal de la aplicación
     * @param name Name of the entry in realtime database
     * @param item Item to update
     */
    public async updateItem(name: string, item: any): Promise<void> {
        this.debugService.debug(DebugLevel.TRACE, 'FirebaseService[ updateItem ] : ', 'init');
        try {
            const collection = this.firestore.collection(name);
            await collection.doc(item.id).update(item);
            this.debugService.debug(DebugLevel.TRACE, 'FirebaseService[ addItem ] /', 'end');
        } catch (error) {
            this.debugService.debug(DebugLevel.DEBUG, 'Error updating firebase item. ', error);
            this.debugService.debug(DebugLevel.ERROR, 'Error updating firebase item. ', null);
        }
    }


    /** Unsubscribe from all service streams */
    public ngOnDestroy(): void {
        this.unsubscriber$.next(true);
        this.unsubscriber$.complete();
    }
}