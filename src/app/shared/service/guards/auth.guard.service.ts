
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Router, CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import {  AuthService } from '@eurega/web-core';

@Injectable()
export class AuthGuard implements CanActivate {
    path: ActivatedRouteSnapshot[];
    route: ActivatedRouteSnapshot;

    constructor(private router: Router, private authService: AuthService) { }
    // TODO incluir reconocimiento de roles
    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        const auth = this.authService.isAuthenticated();
        if (!auth) { this.router.navigate(['/']); }
        return auth;
    }
}
