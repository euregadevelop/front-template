import { Component, Input } from '@angular/core';

@Component({
    selector: 'section-header',
    templateUrl: './section.header.pug',
    styleUrls: ['./section.header.scss']
})
export class SectionHeader {
    @Input() public title?: string;
    @Input() public subTitle?: string;
    constructor() { }
}
