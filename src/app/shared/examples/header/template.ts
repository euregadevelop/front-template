// Dafault value for header template
export const headerTemplate: IHeaderTemplate = {
    colors: {
        '--header-bg-color': 'transparent',
        '--header-link-color': 'var(--text-background-dark)',
        '--header-selected-link-color': 'var(--accent-text-color)',
        '--header-fixed-bg-color': 'var(--background-light)',
        '--header-fixed-selected-link-color': 'var(--dark-blue)',
        '--header-fixed-link-color': 'var(--title-color)',
        '--header-mobile-bg-color': 'var(--background-light)',
        '--header-info-bg-color': 'var(--dark-blue)',
        '--header-info-item-title-color': 'var(--accent-text-color)',
        '--header-info-item-content-color': 'var(--text-background-dark)'
    },
    infoBar: true
};
/** Header template definition */
export declare interface IHeaderTemplate {
    /** Colors used in component */
    colors: HeaderColors;
    /** Opaque filter for background image in component */
    'infoBar'?: boolean;
}

declare interface HeaderColors {
    /**  Background color for normal header */
    '--header-bg-color': string;
    /**  Background color for fixed header  */
    '--header-fixed-bg-color': string;
    /**  Link color when header is fixed on top of the page */
    '--header-fixed-selected-link-color': string;
    /** Link color in fixed header */
    '--header-fixed-link-color': string;
    /** Mobile header background color */
    '--header-mobile-bg-color': string;
    /**  Default link text color */
    '--header-link-color': string;
    /** Activated link header text color */
    '--header-selected-link-color': string;
    /** Background color for information header top  navbar */
    '--header-info-bg-color': string;
    /** title for information entry */
    '--header-info-item-title-color': string;
    /** Content for information entry */
    '--header-info-item-content-color': string;
}
