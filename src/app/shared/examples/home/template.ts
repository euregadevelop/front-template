
import { reduceEachLeadingCommentRange } from 'typescript';
import { bgImageTemplate } from '../../../shared/components/bg-image/template';
import { roundedIconsTemplate } from '../../../shared/components/rounded-icons/template';
import { pricingTemplate } from '../../../shared/components/pricing/template';
import { itemsListTemplate } from '../../../shared/components/items-list/template';
import { portfolioTemplate } from '../../../shared/components/portfolio/template';

export class HomeTemplates {
    public bgImageComponentTemplate: bgImageTemplate = {
        colors: {
            '--title-color': 'var(--text-background-dark)',
            '--subtitle-color': 'var(--text-background-dark)',
            '--text-color': 'var(--text-background-dark)',
        },
        filter: 'medium'
    };
    public roundedIconsComponentTemplate: roundedIconsTemplate = {
        colors: {
            '--background-color-component': 'var(--background-light)',
            '--title-color': 'var(--background-dark)',
            '--subtitle-color': 'var(--background-dark)',
            '--border-color': 'var(--background-dark)',
            '--icon-color': 'var(--background-dark)'
        },
        background: 'medium'
    };

    public pricingComponentTemplate: pricingTemplate = {
        colors: {
            '--item-title-separator': 'var(--dark-blue)',
            '--item-title-color': 'var(--dark-blue)',

        }
    };

    public itemsListComponentTemplate: itemsListTemplate = {
        colors: {
            // '--image-border-color': 'red'
        }
    };

    public portfolioComponentTemplate: portfolioTemplate = {
        colors: {
            '--button-color': 'var(--dark-blue)',
            '--item-title-color': 'var(--dark-blue)'
        }
    };
}

// type a = {
//     b: string,
//     c: number
// };

// const b: Partial<a> = {
// };
