import { Component, OnInit } from '@angular/core';
import { HomeMocks } from './mocks';
import { HomeTemplates } from './template';
import { TBoxesListComponent } from '../../../shared/components/boxes-list/interfaces';
import { TranslateService } from '@ngx-translate/core';
import { TItemsListExplicationComponent, TListType } from '../../../shared/components/items-list-explication/interfaces';
import { TLayered } from '../../typings/common';

@Component({
    selector: 'app-home-example',
    templateUrl: './home.example.pug',
    styleUrls: ['./home.example.scss']
})
export class HomeExampleComponent implements OnInit {
    public mocks = HomeMocks;
    public homeTemplates = new HomeTemplates();
    // Componentes usados en el template
    public boxesListComponent: TBoxesListComponent;
    public listExplicationComponent: TItemsListExplicationComponent;
    constructor(private translateService: TranslateService) { }

    ngOnInit(): void {
        this.initComponents();
    }
    /** Initialize information to load shared components used in this page */
    private initComponents(): void {
        // Boxes list component data
        this.boxesListComponent = {
            boxesListItems: this.mocks.services,
            boxesListOptions: { bgFixed: false, bgOptions: 'repeat', layered: TLayered.none },
            boxesListTitle: this.translateService.instant('web.home.SERVICES_TITLE'),
            boxesListSubTitle: this.translateService.instant('web.home.SERVICES_SUBTITLE'),
            boxesListBgImage: 'assets/images/textures/white_bg.jpg',
        };
        // List explication component
        this.listExplicationComponent = {
            bgImage: '',
            items: this.mocks.listExplication,
            options: {
                listType: TListType.enumList
            },
            template: null
        }
    }

}
