import { RoundedIcon } from './../../../shared/components/rounded-icons/interfaces';
import { IPortfolioItem } from './../../../shared/components/portfolio/interfaces';
import { ItemListElement } from './../../../shared/components/items-list/interfaces';
import { IBoxItem } from './../../../shared/components/boxes-list/interfaces';
import { IPricingItem } from './../../../shared/components/pricing/interfaces';
import { ListExplicationTemplate } from '../../components/items-list-explication/template';
import { IListExplicationItem } from '../../components/items-list-explication/interfaces';


export const HomeMocks: IHomeMocks = {
    roundedIcons: [
        {
            icon: 'verified_user',
            title: {
                en: ' Quality code',
                es: 'Código de calidad',
                ca: 'Còdi de qualitat'
            },
            subtitle: {
                en: ' Eurega rules',
                es: 'Eurega manda',
                ca: 'Eurega power'
            }
        },
        {
            icon: 'trending_up',
            title: {
                en: ' We are on fire !',
                es: 'Estamos que nos salimos',
                ca: 'Estem a tope'
            },
            subtitle: {
                en: ' Long text to try a description',
                es: 'Texto largo para probar una descripción',
                ca: 'Text de descripció'
            }
        }
    ],
    pricingItems: [
        {
            title: {
                en: ' English offer',
                es: 'Oferta 1',
                ca: 'Oferta 1 en català'
            },
            description: {
                en: ' English description',
                es: 'Oferta 1 descripción',
                ca: 'Oferta 1 descripció'
            },
            strong: 'strong text',
            details: [
                {
                    en: ' English detail 1',
                    es: 'Oferta 1 descripción',
                    ca: 'Oferta 1 descripció'
                },
                {
                    en: ' English detail 2',
                    es: 'Oferta 1 descripción',
                    ca: 'Oferta 1 descripció'
                }
            ],
            includes: [
                {
                    en: ' English include 1',
                    es: 'Oferta 1 descripción',
                    ca: 'Oferta 1 descripció'
                },
                {
                    en: ' English include 2',
                    es: 'Oferta 1 descripción',
                    ca: 'Oferta 1 descripció'
                }
            ]
        },
        {
            title: {
                en: ' English offer',
                es: 'Oferta 1',
                ca: 'Oferta 1 en català'
            },
            description: {
                en: ' English description',
                es: 'Oferta 1 descripción',
                ca: 'Oferta 1 descripció'
            },
            strong: 'strong text',
            details: [
                {
                    en: ' English detail 1',
                    es: 'Oferta 1 descripción',
                    ca: 'Oferta 1 descripció'
                },
                {
                    en: ' English detail 2',
                    es: 'Oferta 1 descripción',
                    ca: 'Oferta 1 descripció'
                }
            ],
            includes: [
                {
                    en: ' English include 1',
                    es: 'Oferta 1 descripción',
                    ca: 'Oferta 1 descripció'
                },
                {
                    en: ' English include 2',
                    es: 'Oferta 1 descripción',
                    ca: 'Oferta 1 descripció'
                }
            ]
        }
    ],
    portfolioItems: [
        {
            image: {
                src: 'assets/images/sala1.png',
                alt: 'Sala 1'
            },
            description: {
                en: 'First item',
                es: 'Item primero',
                ca: 'Primer item'
            },
            title: {
                en: 'First title',
                es: 'Título primero',
                ca: 'Primer titol'
            },
            logo: {
                src: 'assets/images/logo_header.png',
                alt: 'logo alt text'
            },
            link: 'http://google.com.es'
        },
        {
            image: {
                src: 'assets/images/sala2.png',
                alt: 'Sala 2'
            },
            description: {
                en: 'First item',
                es: 'Item segundo',
                ca: 'Primer item'
            },
            title: {
                en: 'First title',
                es: 'Título segundo',
                ca: 'Primer titol'
            },
            logo: {
                src: 'assets/images/logo_header.png',
                alt: 'logo alt text'
            },
            link: 'http://google.com.es'
        },
        {
            image: {
                src: 'assets/images/sala3.png',
                alt: 'image alt text'
            },
            description: {
                en: 'Third item',
                es: 'Item tercero',
                ca: 'Tercer item'
            },
            title: {
                en: 'Third title',
                es: 'Título tercero',
                ca: 'Primer titol'
            },
            logo: {
                src: 'assets/images/logo_header.png',
                alt: 'logo alt text'
            },
            link: 'http://google.com.es'
        },
        {
            image: {
                src: 'assets/images/sala4.png',
                alt: 'Sala 4'
            },
            logo: {
                src: 'src/assets/images/logo_header.png',
                alt: 'logo alt text'
            },
            description: {
                en: 'Second item',
                es: 'Item segundo',
                ca: 'Segon item'
            },
            title: {
                en: 'Second title',
                es: 'Título segundo',
                ca: 'Segon titol'
            }
        }
    ],
    itemListItems: [
        {
            image: {
                src: 'assets/images/logo_header.png',
                alt: 'logo alt text'
            },
            description: {
                en: 'Second item',
                es: 'Item segundo',
                ca: 'Segon item'
            },
            title: {
                en: 'Second item',
                es: 'Item segundo',
                ca: 'Segon item'
            }
        },
        {
            image: {
                src: 'assets/images/logo_header.png',
                alt: 'logo alt text'
            },
            description: {
                en: 'Second item',
                es: 'Item segundo',
                ca: 'Segon item'
            },
            title: {
                en: 'Second item',
                es: 'Item segundo',
                ca: 'Segon item'
            }
        }
    ],
    services: [
        {
            title: {
                en: 'Cementiris',
                es: 'Cementiris',
                ca: 'Cementiris'
            },
            description: {
                en: 'Second item',
                es: 'Item segundo',
                ca: 'Segon item'
            },
            rel: 'what',
            image: {
                src: 'assets/images/sections/servei3.PNG',
                alt: 'Servei 1'
            }
        },
        {
            title: {
                en: 'Flors',
                es: 'Flors',
                ca: 'Flors'
            },
            description: {
                en: 'Second item',
                es: 'Item segundo',
                ca: 'Segon item'
            },
            rel: 'what',
            image: {
                src: 'assets/images/sections/servei2.PNG',
                alt: 'Servei 1'
            }
        },
        {
            title: {
                en: 'Sales',
                es: 'Sales',
                ca: 'Sales'
            },
            description: {
                en: 'Second item',
                es: 'Item segundo',
                ca: 'Segon item'
            },
            rel: 'what',
            image: {
                src: 'assets/images/sections/servei1.PNG',
                alt: 'Servei 1'
            }
        }
    ],
    listExplication: [
        {
            description: {
                es: 'lorem ipsum description textum, alegrum ... ',
                en: 'lorem ipsum description textum, alegrum ... '
            },
            img: {
                src: 'assets/images/sala1.png',
                alt: 'Imágen de prueba'
            },
            title: {
                es: 'Título en esp ... ',
                en: 'Title in english ... '
            }
        }
    ]
};


declare interface IHomeMocks {
    roundedIcons: Array<RoundedIcon>;
    pricingItems: Array<IPricingItem>;
    portfolioItems: Array<IPortfolioItem>;
    itemListItems: Array<ItemListElement>;
    services: Array<Partial<IBoxItem>>;
    listExplication: Array<Partial<IListExplicationItem>>;
}
