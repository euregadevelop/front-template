import { Component, OnInit } from '@angular/core';
import { DebugService, DebugLevel } from '@eurega/web-core';
import { CoreService } from './web/services/core.service';

@Component({
    selector: 'app-root',
    template: `<router-outlet></router-outlet>`
})
export class RootComponent implements OnInit {

    constructor(private logger: DebugService, private coreService: CoreService) { }
    /** Global application init */
    public async ngOnInit(): Promise<void> {
        try {
            await this.coreService.initApp();
        } catch (error) {
            this.logger.debug(DebugLevel.ERROR, 'Error initializing app. ', error);
        }
    }
}
