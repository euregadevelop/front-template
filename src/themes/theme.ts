export const templates: any = {
    lightTheme: {
        colors: {
            /** Colores de fondo */
            '--background-light': 'var(--grey-light)',
            '--background-dark': 'var(--grey)',
            /** Colores de textos en función de su fondo  */
            '--text-background-default': 'var(--grey)',
            '--text-background-light': 'var(--grey)',
            '--text-background-dark': '#FFF', // 'var(--grey-light)',
            /** Colores generales para texto */
            '--title-color': "var(--grey-dark)",
            '--subtitle-color': "var(--grey-dark)",
            '--plain-text-color': "var(--grey-dark)",
            '--accent-text-color': "var(--light-blue)",
            /** Colores corporativos */
            '--primary': '#389BFF',
            '--text-primary': '#fff',
            '--secondary': '#B2B4B7',
            '--text-secondary': '#fff',
            /** Utilidades */
            '--error': '#EB5945',
            '--text-error': '#fff',
            '--warning': '#F6BB42',
            '--text-warning': '#fff'
        }
    },
    darkTheme: {
        colors: {
            '--background-light': '#EFF0F4',
            '--background-dark': '#ccc',
            '--on-background': '#0F0F0F',
            '--primary': '#389BFF',
            '--text-primary': '#fff',
            '--secondary': '#B2B4B7',
            '--text-secondary': '#fff',
            '--error': '#EB5945',
            '--text-error': '#fff',
            '--warning': '#F6BB42',
            '--text-warning': '#fff'
        }
    }
};


export declare interface EuregaTheme {
    '--background-light': string;
    '--background-dark': string;
    '--text-background': string;
    // Colores de textos en función de su background
    '--text-background-light': string;
    '--text-background-dark': string;
}
