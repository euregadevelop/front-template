import { DebugLevel } from '@eurega/web-core';

export const environment = {
    production: true,
    firebase: {
        apiKey: "AIzaSyCyMRGf6RHAUP-jXop40s2_lroge_gVIIk",
        authDomain: "eurega-3af9d.firebaseapp.com",
        databaseURL: "https://eurega-3af9d.firebaseio.com",
        projectId: "eurega-3af9d",
        storageBucket: "eurega-3af9d.appspot.com",
        messagingSenderId: "1005315369185",
        appId: "1:1005315369185:web:79ac763cb70db675bb6fa0"
    },
    APP_LOCALSTORAGE_PREFIX: 'eure',
    DebugLevel: DebugLevel.ERROR,
};
