// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { DebugLevel } from '@eurega/web-core';

export const environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyCyMRGf6RHAUP-jXop40s2_lroge_gVIIk",
        authDomain: "eurega-3af9d.firebaseapp.com",
        databaseURL: "https://eurega-3af9d.firebaseio.com",
        projectId: "eurega-3af9d",
        storageBucket: "eurega-3af9d.appspot.com",
        messagingSenderId: "1005315369185",
        appId: "1:1005315369185:web:79ac763cb70db675bb6fa0"
    },
    DebugLevel: DebugLevel.DEBUG,
    ImageBucket: 'https://s3-eu-west-1.amazonaws.com/dev.assets.pujols.cat/',
    apiURL: 'https://2hqtdit5ui.execute-api.eu-west-1.amazonaws.com/dev',
    APP_LOCALSTORAGE_PREFIX: 'eure'
};
