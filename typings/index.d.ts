export declare interface i18nField {
    en?: string,
    es?: string,
    ca?: string
}
